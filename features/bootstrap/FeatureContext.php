<?php

require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Config.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Integrator.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Request/PaymentRequest.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Response/VerificationResponse.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Api/Adapter/Curl.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Api/Request/AuthRequest.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Response/AuthorizationResponse.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Exception/Exception.php';
require_once  dirname(__FILE__).'/../../lib/ThreeDS/Integrator/Exception/RunTimeException.php';

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * @var \ThreeDS\Integrator\Config
     */
    protected $config;
    /**
     * @var \ThreeDS\Integrator\Integrator
     */
    protected $integrator;
    /**
     * @var array
     */
    protected $_POST;
    /**
     * @var string
     */
    protected $htmlString;
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->_POST = array();
        global $_POST;
    }

    /**
     * @Given I have a credentials :arg1 :arg2
     */
    public function iHaveACredentials($arg1, $arg2)
    {
        $this->config = new \ThreeDS\Integrator\Config($arg1,$arg2);
    }

    /**
     * @Given I'm pointing to the :arg1 environment
     */
    public function iMPointingToTheEnvironment($arg1)
    {
        if ($arg1 == 'demo') {
            $this->config->setDemo(true);
        }
    }

    /**
     * @When I make an authentication request
     */
    public function iMakeAnAuthenticationRequest()
    {
        $_POST = $this->_POST;
        $this->integrator = new \ThreeDS\Integrator\Integrator(
            $this->config,new \ThreeDS\Integrator\Api\Adapter\Curl(),
            new \ThreeDS\Integrator\Request\PaymentRequest($this->_POST),
            new \ThreeDS\Integrator\Response\VerificationResponse\VerificationResponse()
        );

        $this->htmlString = $this->integrator->requestAuthorization();
    }

    /**
     * @When the post has a field :arg1 with :arg2
     */
    public function thePostHasAFieldWith($arg1, $arg2)
    {
        $this->_POST[$arg1] = $arg2;
    }

    /**
     * @Then I should get an html string
     */
    public function iShouldGetAnHtmlString()
    {
        if (empty($this->htmlString)) {
            throw new Exception('No html response received');
        }
    }

    /**
     * @Then the html should have :arg1
     */
    public function theHtmlShouldHave($arg1)
    {
        if (strpos($this->htmlString,$arg1) === FALSE) {
            throw new Exception("Couldn't find string '$arg1'");
        }
    }

    
}
