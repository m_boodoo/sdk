Feature: Make authentication request
  Scenario: Make authentication request
    Given I have a credentials "api_key" "api_secret"
    And I'm pointing to the "demo" environment
    And the post has a field "x_card_num" with "4111111111111112"
    And the post has a field "x_exp_month" with "08"
    And the post has a field "x_exp_year" with "18"
    And the post has a field "x_amount" with "1.99"
    And the post has a field "x_transaction_id" with "t-12314"
    And the post has a field "x_message_id" with "mid-123123"
    And the post has a field "x_relay_url" with "http://local.paay.co"
    When I make an authentication request
    Then I should get an html string
    And the html should have "iframe"
    And the html should have "var form = document.getElementById('verification-form');"