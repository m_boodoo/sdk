<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator;

/**
 * Class Config
 * @package ThreeDS\Integrator
 */
class Config
{
    /**
     * @var string
     */
    protected $apiKey;
    /**
     * @var string
     */
    protected $apiSecret;
    /**
     * @var string
     */
    protected $uri;
    /**
     * @var boolean
     */
    protected $demo;
    /**
     * Iframe timeout in seconds
     *
     * @var int
     */
    protected $timeout = 10;
    /**
     * Sometimes the url of the integrator script cannot be "auto-dsicovered". So it can be set
     *
     * @var string
     */
    protected $integratorUrl;
    /**
     * Wether to hide iframe or not
     *
     * @var boolean
     */
    protected $hideForm = true;

    protected $allowProcessing = true;

    public function __construct($apiKey,$apiSecret,$mpiUrl='https://mpi.3dsintegrator.com')
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->uri = rtrim($mpiUrl,'/');
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @param string $apiSecret
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return boolean
     */
    public function isDemo()
    {
        return $this->demo;
    }

    /**
     * @param boolean $demo
     */
    public function setDemo($demo)
    {
        $this->demo = $demo;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @return int
     */
    public function getTimeoutInMilliseconds()
    {
        return $this->timeout * 1000;
    }

    public function getFullEndpoint($endPoint)
    {
        return $this->isDemo() ? $this->uri.'/index_demo.php'.$endPoint  :  $this->uri.$endPoint;
    }

    /**
     * @param mixed $integratorUrl
     */
    public function setIntegratorUrl($integratorUrl)
    {
        $this->integratorUrl = $integratorUrl;
    }

    /**
     * @return string
     */
    public function getIntegratorUrl()
    {
        if (!isset($this->integratorUrl)) {
            list($prot)	= explode('/',strtolower($_SERVER['SERVER_PROTOCOL']));
            $s		= isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']  == 'on' ? 's' : '';
            $port		= $_SERVER['SERVER_PORT'] == '80' || $_SERVER['SERVER_PORT'] == '443' ? '' : ':'.$_SERVER['SERVER_PORT'];
            $this->integratorUrl = $prot.$s.'://'.$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
        }

        return $this->integratorUrl;
    }

    /**
     * @return boolean
     */
    public function isHideForm()
    {
        return $this->hideForm;
    }

    /**
     * @param boolean $hideForm
     */
    public function setHideForm($hideForm)
    {
        $this->hideForm = $hideForm;
    }

    /**
     * @return boolean
     */
    public function isAllowProcessing()
    {
        return $this->allowProcessing;
    }

    /**
     * @param boolean $allowProcessing
     */
    public function setAllowProcessing($allowProcessing)
    {
        $this->allowProcessing = $allowProcessing;
    }


}