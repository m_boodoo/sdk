<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace ThreeDS\Integrator\Response;
use ThreeDS\Integrator\Response\AbstractVerifiedResponse;


class VerificationResponse extends AbstractVerifiedResponse
{
    public function __construct($response=null)
    {
        $this->rawResponse = $response;
    }

    public function getSubmissionData()
    {
        return array(
            'xid' => $this->getXid(),
            'eci' => $this->getEci(),
            'cavv' => $this->getCavv(),
            'status' => $this->getStatus()
        );
    }

}