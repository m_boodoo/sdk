<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Response;

use ThreeDS\Integrator\Exception\RunTimeException;

abstract class AbstractVerifiedResponse extends AbstractResponse
{
    public function getXid()
    {
        return $this->getValue('xid');
    }

    public function getTime()
    {
        return $this->getValue('time');
    }

    public function getCavvAlgorithm()
    {
        return $this->getValue('cavv_algorithm');
    }

    public function getEci()
    {
        return $this->getValue('eci');
    }

    public function getCavv()
    {
        return $this->getValue('cavv');
    }

    public function getStatus()
    {
        return $this->getValue('status');
    }

    public function isSuccess()
    {
        return $this->getValue('success');
    }

    public function getError()
    {
        return !$this->isSuccess() ? $this->getValue('error') : null;
    }

    /**
     * @param mixed $rawResponse
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
    }
    /**
     * This is an array of fields that should be passed to the script that handles payment processing
     *
     * @return array
     */
    abstract public function getSubmissionData();
}