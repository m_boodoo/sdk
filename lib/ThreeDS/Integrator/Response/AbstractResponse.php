<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Response;


use ThreeDS\Integrator\Exception\Exception;
use ThreeDS\Integrator\Exception\RunTimeException;

abstract class AbstractResponse
{
    protected $rawResponse;

    public function __get($name)
    {
        return $this->getValue($name);
    }

    protected function getValue($field)
    {
        if (!$this->rawResponse || !is_object($this->rawResponse)) {
            throw new RunTimeException("Invalid validation response");
        }
        if (isset($this->rawResponse->{$field})) {
            return $this->rawResponse->{$field};
        } else {
            throw new Exception("Field '$field' was not part of response");
        }
    }
}