<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Request;


interface IAuthorizationRequest
{
    /**
     * @return float
     */
    public function getAmount();

    /**
     * @return string
     */
    public function getCardNumber();

    /**
     * @return integer
     */
    public function getExpirationMonth();

    /**
     * @return integer
     */
    public function getExpirationYear();

    /**
     * @return string
     */
    public function getTransactionId();

    /**
     * @return string
     */
    public function getMessageId();

    /**
     * @return string
     */
    public function getRelayUrl();
}