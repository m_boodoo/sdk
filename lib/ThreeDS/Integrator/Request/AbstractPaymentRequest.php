<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Request;


use ThreeDS\Integrator\Exception\Exception;

abstract class AbstractPaymentRequest
{
    protected $data = array();
    protected $verificationResponse;

    protected function getValue($field)
    {
        if (isset($this->data[$field])) {
            return $this->data[$field];
        } else {
            throw new Exception("Field '$field' was not part of request'");
        }
    }

    abstract public function getAmount();
    abstract public function getCardNumber();
    abstract public function getExpirationMonth();
    abstract public function getExpirationYear();
    abstract public function getTransactionId();
    abstract public function getMessageId();
    abstract public function getRelayUrl();

    public function getData()
    {
        return $this->data;
    }

    public function getSubmissionData()
    {
        $data = $this->data;
        unset($data['x_relay_url']);
        unset($data['___verify_pares']);
        unset($data['shared_pares']);
        return $data;
    }
}