<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Request;

use ThreeDS\Integrator\Exception\Exception;

class PaymentRequest extends AbstractPaymentRequest    implements IAuthorizationRequest
{

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getAmount()
    {
        return $this->getValue('x_amount');
    }

    public function getCardNumber()
    {
        return $this->getValue('x_card_num');
    }

    public function getExpirationMonth()
    {
        return $this->getValue('x_exp_month');
    }

    public function getExpirationYear()
    {
        return $this->getValue('x_exp_year');
    }

    public function getTransactionId()
    {
        return $this->getValue('x_transaction_id');
    }

    public function getMessageId()
    {
        return $this->getValue('x_message_id');
    }

    public function getRelayUrl()
    {
        return $this->getValue('x_relay_url');
    }

    public function getData()
    {
        return $this->data;
    }
}