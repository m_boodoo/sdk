<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator;


use ThreeDS\Integrator\Api\Adapter\AbstractAdapter;
use ThreeDS\Integrator\Api\Request\VerificationRequest;
use ThreeDS\Integrator\Request\AbstractPaymentRequest;
use ThreeDS\Integrator\Response\AbstractVerifiedResponse;

class Integrator
{
    /**
     * Configuration
     *
     * @var Config
     */
    protected $config;
    /**
     * @var AbstractPaymentRequest
     */
    protected $paymentRequest;
    /**
     * @var AbstractVerifiedResponse
     */
    protected $verifiedResponse;
    /**
     * @var AbstractAdapter
     */
    protected $adapter;
    protected $currentUrl;

    public function __construct(
        Config $config,
        AbstractAdapter $abstractAdapter,
        AbstractPaymentRequest $paymentRequest,
        AbstractVerifiedResponse $verifiedResponse,
        $currentUrl=null
    )
    {
        $this->config = $config;
        $this->adapter = $abstractAdapter;
        $this->adapter->setConfig($this->config);

        $this->paymentRequest = $paymentRequest;
        $this->verifiedResponse = $verifiedResponse;
        $this->currentUrl = $currentUrl ? $currentUrl : $this->config->getIntegratorUrl();
    }

    public function requestAuthorization()
    {
        /**
         * Make authentication request
         */
        $authRequest = new \ThreeDS\Integrator\Api\Request\AuthRequest($this->paymentRequest,$this->config);

        try {
            $authRequestResponse = new \ThreeDS\Integrator\Response\AuthorizationResponse($this->adapter->sendRequest($authRequest));
            $escapedPares = str_replace(array("\r", "\n"), '', $authRequestResponse->PaReq);
            $timeout = $this->config->getTimeoutInMilliseconds();


            $html = '';
            if($this->config->isHideForm()) {
                $html.=' <style>
                iframe {
                    display: none;
                }
            </style>';
            }
            $html.="<iframe id=\"frame\" src=\"about:blank\"></iframe>
    <form id=\"verification-form\" action=\"{$this->currentUrl}\" method=\"post\">
        <input type=\"hidden\" id=\"pares\" name=\"shared_pares\" />
        <input type=\"hidden\" name=\"___verify_pares\" value=\"1\" />";

        foreach ($this->paymentRequest->getData() as $key=>$val) {
            if (is_array($val)) {
                foreach ($val as $arrayValue) {
                    $html.= "<input type=\"hidden\" name=\"$key\" value=\"$arrayValue\" />";
                }
            } else {

                $html.= "<input type=\"hidden\" name=\"$key\" value=\"$val\" />";
            }
        }
            
        $timeOutSubmitTrigger = $this->config->isAllowProcessing() ? "document.createElement('form').submit.call(form);" : '';


        $html.="</form>

    <script type=\"application/javascript\">
        (function(){
            var frame = document.getElementById('frame');
            var form = document.getElementById('verification-form');
            

            frame.contentDocument.write('<html> <body> <form name=\"form3ds\" action=\"$authRequestResponse->AcsUrl\" method=\"post\"> <input name=\"PaReq\" type=\"hidden\" value=\"$escapedPares\"> <input name=\"MD\" type=\"hidden\" value=\"$authRequestResponse->MD;\"> <input name=\"TermUrl\" type=\"hidden\" value=\"$this->currentUrl\"> </form> </body> </html>');
            frame.contentDocument.form3ds.submit();

             var tid = setTimeout( function(){
                form.action = \"$authRequestResponse->TermUrl\";
                var elem = document.getElementById(\"pares\");
                elem.parentNode.removeChild(elem);
                $timeOutSubmitTrigger
                console.log('timed out after $timeout');
            }, $timeout );
            
            window.onmessage = function(e){
                if (e.data.PaRes != '') {//do some better verfification here
                    j = e.data;
                    var pinput = document.getElementById('pares');
                    pinput.value = j;
                    document.createElement('form').submit.call(form);
                    console.log('response received');
                    clearInterval(tid);
                }
            };
        })();
    </script>";

            return $html;

        } catch (\ThreeDS\Integrator\Exception\RunTimeException $e) {
            $this->submitPayment($this->paymentRequest,null,$e->getMessage());
        }
    }

    public function shareAuthorizationResponse(array $data)
    {
        return '<script type=\'application/javascript\'>window.top.postMessage('.json_encode($data['PaRes']).', \'*\')</script>';
    }

    public function verifyAuthorizationResponse(
    ) {
        $pares = $_POST['shared_pares'];
        $verificationRequest = new VerificationRequest($this->paymentRequest,$pares,$this->config);
        try {

            $tresponse = $this->adapter->sendRequest($verificationRequest);
            $this->verifiedResponse->setRawResponse($tresponse);
            $this->submitPayment($this->paymentRequest,$this->verifiedResponse,$this->verifiedResponse->getError());
        } catch (\ThreeDS\Integrator\Exception\RunTimeException $e) {
            $this->submitPayment($this->paymentRequest,null,$e->getMessage());
        }
    }

    public function submitPayment(
        AbstractPaymentRequest $authRequest,
        AbstractVerifiedResponse $verifiedResponse=null,
        $errorMessage=null
    )
    {
        if (!$this->config->isAllowProcessing()) {
            var_dump($authRequest->getData());die();
        }
        $form = '<html>
        <head></head>
        <body onload="document.createElement(\'form\').submit.call(document.form3ds);">
        <form name="form3ds" action="'.$authRequest->getRelayUrl().'" method="post">
        ';
        if ($errorMessage) {
           $form.='<input type="hidden" name="3dsecure_error" value="'.$errorMessage.'" />';
        }

        foreach ($authRequest->getSubmissionData() as $key=>$value) {
            $form .= "<input type=\"hidden\" name=\"$key\" value=\"$value\" />";
        }

        if ($verifiedResponse) {
            foreach ($verifiedResponse->getSubmissionData() as $key=>$value) {
                $form .= "<input type=\"hidden\" name=\"$key\" value=\"$value\" />";
            }
        }

        $form.='</form>
        </body>
        </html>';

        echo $form;
    }

    public function render()
    {
        if (isset($_POST['PaRes'])) {
            return $this->shareAuthorizationResponse($_POST);
        } elseif (isset($_POST['___verify_pares']) && $_POST['___verify_pares'] == 1) {
            $this->verifyAuthorizationResponse();
        } elseif (isset($_POST['x_relay_url'])) {
            return $this->requestAuthorization();
        }
    }

}