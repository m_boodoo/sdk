<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Api\Adapter;


use ThreeDS\Integrator\Api\Request\IRequest;
use ThreeDS\Integrator\Config;

abstract class AbstractAdapter
{
    /**
     * @var Config
     */
    protected $config;

    public function sendRequest(IRequest $request)
    {
        return $this->makeCall($request->getEndPoint(),$request->getData());
    }

    /**
     * Get signature that all mpi calls should have
     *
     * @param $endpoint
     * @param array $data
     * @return string
     */
    public function getSignature($endpoint,array $data)
    {
        ksort($data);
        return hash('sha256',$this->config->getApiKey().$endpoint.json_encode( $data, JSON_UNESCAPED_SLASHES ).$this->config->getApiSecret());
    }

    /**
     * @param $endpoint
     * @param array $data
     * @throws \RuntimeException
     *
     * @return array
     */
    abstract protected function makeCall($endpoint,array $data);

    /**
     * @param Config $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


}