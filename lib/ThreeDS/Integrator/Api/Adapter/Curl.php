<?php
/*
* This file is part of the 3DS Integrator.
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace ThreeDS\Integrator\Api\Adapter;

use ThreeDS\Integrator\Config;
use ThreeDS\Integrator\Exception\RunTimeException;

/**
 * Curl adapter used to make calls to mpi
 *
 * @package ThreeDS\Integrator\Api\Adapter
 */
class Curl extends AbstractAdapter
{
    private $lastEndpoint = '';
    private $lastSignature = '';
    private $lastData;

    public function __construct(Config $config = null)
    {
        $this->config = $config;
    }

    /**
     * Call api using curl
     *
     * @param $endpoint
     * @param array $data
     * @throws RunTimeException
     *
     * @return mixed
     */
    public function makeCall($endpoint, array $data)
    {
        $curl 		= curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->config->getFullEndpoint($endpoint),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode( $data ),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "x-mpi-api-key: ".$this->config->getApiKey(),
                "x-mpi-signature: ".$this->getSignature($this->config->getFullEndpoint($endpoint),$data)
            ),
        ));

        $this->lastData = $data;
        $this->lastEndpoint = $this->config->getUri().$endpoint;
        $this->lastSignature = $this->getSignature($this->config->getFullEndpoint($endpoint),$data);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            throw new RunTimeException($err);
        }

        $resp = json_decode($response);

        if (isset($resp->error) && !empty($resp->error)) {
            throw new RunTimeException($resp->error);
        }

        return $resp;
    }

    /**
     * @return string
     */
    public function getLastEndpoint()
    {
        return $this->lastEndpoint;
    }

    /**
     * @return string
     */
    public function getLastSignature()
    {
        return $this->lastSignature;
    }

    /**
     * @return mixed
     */
    public function getLastData()
    {
        return $this->lastData;
    }



}