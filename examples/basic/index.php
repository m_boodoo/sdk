<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <style>
        .container-fluid {
            padding-top: 40px;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
            <h3>Billing Form</h3>
            <form action="/examples/basic/integration.php" method="post">
                <input type="hidden" name="x_relay_url" value="/examples/basic/confirmation.php">
                <div class="form-group">
                    <label>Amount</label>
                    <input type="text" name="x_amount" value="200" />
                </div>
                <p>
                    <label>Card No.</label>
                    <input type="text" name="x_card_num" value="4111111111111112" />
                </p>
                <p>
                    <label>Card Expiration Month</label>
                    <input type="text" name="x_exp_month" value="12" />
                </p>
                <p>
                    <label>Card Expiration Year</label>
                    <input type="text" name="x_exp_year" value="18" />
                </p>
                <p>
                    <label>First Name</label>
                    <input type="text" name="first_name" value="Foo" />
                </p>
                <p>
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="Bar" />
                </p>
                <input type="hidden" name="x_transaction_id" value="transaction-<?=uniqid();?>" />
                <input type="hidden" name="x_message_id" value="message-<?=uniqid();?>" />
                <button>Submit</button>
            </form>
            </div>
        </div>

    </div><!-- /.container -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
