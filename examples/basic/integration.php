<?php include_once '../../autoload.php';?>

<?php
$config = new \ThreeDS\Integrator\Config('api_key','api_secret');
$config->setDemo(true);
$config->setTimeout(12);


$api = new \ThreeDS\Integrator\Api\Adapter\Curl();
$payment = new \ThreeDS\Integrator\Request\PaymentRequest($_POST);
$responseHandler = new \ThreeDS\Integrator\Response\VerificationResponse();
$integrator = new \ThreeDS\Integrator\Integrator($config,$api,$payment,$responseHandler);

;?>

<?php echo $integrator->render();?>