<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href=".">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<style>
    .container-fluid {
        padding-top: 40px;
    }
</style>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1><Confirmation></Confirmation></h1>
        <p>Below are all the post variables that was posted to the processing.</p>
    </div>
</div>

<div class="container">
    <table class="table">
        <?php foreach ($_POST as $key=>$value):?>
            <tr>
                <td><?php echo $key;?></td>
                <td><?php echo $value;?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>

<?php
//POST to authorize.net
$post_url = "https://secure.authorize.net/gateway/transact.dll";
$loginid = 'ENTER_LOGIN_ID';
$transactionkey = 'ENTER_TRANSACTION_KEY';
$post_values = array(

    // the API Login ID and Transaction Key must be replaced with valid values
    "x_login"			=> $loginid,
    "x_tran_key"		=> $transactionkey,
    "x_duplicate_window"=> 28800,

    "x_version"			=> "3.1",
    "x_delim_data"		=> "TRUE",
    "x_delim_char"		=> "|",
    "x_relay_response"	=> "FALSE",
    "x_invoice_num"     => uniqid(),

    "x_type"			=> "AUTH_ONLY",
    "x_method"			=> "CC",
    "x_card_num"		=> $_POST['ccNum'],
    "x_exp_date"		=> $_POST['expDate'],
    "x_auth_code"		=> $_POST['cvv'],

    "x_amount"			=> "1",
    "x_description"		=> $description,

    "x_first_name"		=> $_POST['firstName'],
    "x_last_name"		=> $_POST['lastName'],
    "x_address"			=> $_POST['street'],
    "x_city"			=> $_POST['city'],
    "x_state"			=> $_POST['state'],
    "x_zip"				=> $_POST['zip'],
    "x_email"			=> $_POST['email']

);
//include the 3ds verification data if available
if (isset($_POST['eci']) && isset($_POST['cavv'])) {
    $post_values['x_authentication_indicator'] = $_POST['eci'];
    $post_values['x_cardholder_authentication_value'] = $_POST['cavv'];
}


$post_string = "";
foreach( $post_values as $key => $value )
{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
$post_string = rtrim( $post_string, "& " );

$request = curl_init($post_url); // initiate curl object
curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
$post_response = curl_exec($request); // execute curl post and store results in $post_response

curl_close ($request);

$response_array = explode('|',$post_response);
var_dump($response_array);

;?>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
