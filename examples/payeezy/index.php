<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <style>
        .container-fluid {
            padding-top: 40px;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
            <h3>Billing Form</h3>
            <form action="/examples/payeezy/integration.php" method="post">
                <input type="hidden" name="x_relay_url" value="/examples/payeezy/payeezy.php">
                <p>
                    <label>Card No.</label>
                    <input type="text" name="ccNum" value="4788250000028291" placeholder="enter valid credit card"/>
                </p>
                <p>
                    <label>Expiration</label>
                    <input type="text" name="expDate" value="1020" placeholder="e.g. 1219" />
                </p>
                <p>
                    <label>CVV</label>
                    <input type="text" name="cvv" value="123" />
                </p>
                <p>
                    <label>First Name</label>
                    <input type="text" name="firstName" value="Foo" />
                </p>
                <p>
                    <label>Last Name</label>
                    <input type="text" name="lastName" value="Bar" />
                </p>
                <p>
                    <label>Street</label>
                    <input type="text" name="street" value="" />
                </p>
                <p>
                    <label>City</label>
                    <input type="text" name="city" value="" />
                </p><p>
                    <label>State</label>
                    <input type="text" name="state" value="" />
                </p>
                <p>
                    <label>Zip</label>
                    <input type="text" name="zip" value="" />
                </p>
                <p>
                    <label>Email</label>
                    <input type="email" name="email" value=""  />
                </p>
                <button>Submit</button>
            </form>
            </div>
        </div>

    </div><!-- /.container -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
