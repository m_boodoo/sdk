<?php include_once '../../autoload.php';?>

<?php
 //define custom payment request
class MyPaymentRequest extends \ThreeDS\Integrator\Request\AbstractPaymentRequest
{
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getAmount() {
        return 1;
    }
    public function getCardNumber() {
        return $this->getValue('ccNum');
    }
    public function getExpirationMonth() {
        return substr($this->getValue('expDate'),0,2);
    }
    public function getExpirationYear() {
        return substr($this->getValue('expDate'),-2);
    }
    public function getTransactionId() {
        return '5818a6d7aa22a';
    }
    public function getMessageId() {
        return '5818a6d7aa22a';
    }

    public function getRelayUrl()
    {
        return $this->getValue('x_relay_url');
    }

}
?>

<?php
$config = new \ThreeDS\Integrator\Config('Ma9NP9BrJnEUXUxTODwWEiJb4mITijMU','kLy1L0ikdM61ysKcW7YqfysaL1TTbxEm83dKSlUX');
//$config->setDemo(true);
$config->setTimeout(12000);


$api = new \ThreeDS\Integrator\Api\Adapter\Curl($config);
$payment = new MyPaymentRequest($_POST);
$responseHandler = new \ThreeDS\Integrator\Response\VerificationResponse();
$integrator = new \ThreeDS\Integrator\Integrator($config,$api,$payment,$responseHandler);

;?>

<?php echo $integrator->render();?>