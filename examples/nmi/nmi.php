<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href=".">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<style>
    .container-fluid {
        padding-top: 40px;
    }
</style>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1><Confirmation></Confirmation></h1>
        <p>Below are all the post variables that was posted to the processing.</p>
    </div>
</div>

<div class="container">
    <table class="table">
        <?php foreach ($_POST as $key=>$value):?>
            <tr>
                <td><?php echo $key;?></td>
                <td><?php echo $value;?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>

<?php
//POST to nmi

function doSale($amount, $ccnumber, $ccexp,$status=null, $xid=null,$eci=null,$cavv=null) {

    $query  = "";
    // Login Information
    $query .= "username=" . urlencode('ENTER_USERNAME') . "&";
    $query .= "password=" . urlencode('ENTER_PASSWORD') . "&";
    // Sales Information
    $query .= "ccnumber=" . urlencode($ccnumber) . "&";
    $query .= "ccexp=" . urlencode($ccexp) . "&";
    $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
    // Billing Information
    if ($status) {
        switch ($status)
        {
            case 'Y':
                $query .= "cardholder_auth=" . urlencode('verified') . "&";
                break;
            case 'A':
                $query .= "cardholder_auth=" . urlencode('attempted') . "&";
                break;
        }

    }

    if ($xid) {
        $query .= "xid=" . urlencode($xid) . "&";
    }
    if ($eci) {
        $query .= "eci=" . urlencode($eci) . "&";
    }
    if ($cavv) {
        $query .= "cavv=" . urlencode($cavv) . "&";
    }



    // Shipping Information
    $query .= "type=sale";
    return _doPost($query);
}


function _doPost($query) {
    $responses = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://secure.networkmerchants.com/api/transact.php");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
    curl_setopt($ch, CURLOPT_POST, 1);

    if (!($data = curl_exec($ch))) {
        return ERROR;
    }
    curl_close($ch);
    unset($ch);
    print "\n$data\n";
    $data = explode("&",$data);
    for($i=0;$i<count($data);$i++) {
        $rdata = explode("=",$data[$i]);
        $responses[$rdata[0]] = $rdata[1];
    }
    return $responses['response'];
}

if($_POST['status'] == 'N') {
    //proceed without 3DSecure
    echo 'NO 3D Secure';
} else {

    $result = doSale(1,$_POST['ccNum'],$_POST['expDate'],$_POST['status'],$_POST['xid'],$_POST['eci'],$_POST['cavv']);
    var_dump($result);
}


;?>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
